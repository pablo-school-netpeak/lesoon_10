<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lesson extends CI_Controller {

  public function __construct()
  {
          parent::__construct();
          $this->load->model('lesson_model');
          $this->load->library('session');
  }
	public function index()
	{
    $this->load->helper('lesson_helper');


    echo newHelper_method('Try to test new helper');

    $posts =  $this->lesson_model->get_news();

    $data = array(
      'posts' => $posts,
    );

		$this->load->view('lesson/index_view', $data);
	}

  public function showNews($id = NULL)
  {
    $news_item =  $this->lesson_model->get_news($id);

    $data = array(
      'news_item' => $news_item,
    );

    $this->load->view('lesson/showNews_view', $data);
  }

  public function insertNews($title = '')
  {
    $this->lesson_model->insert_news($title);

  }

  public function deleteNews($id = NULL)
  {
    $this->lesson_model->delete_news($id);
  }

  public function updateNews($id = NULL, $title = '')
  {

    $this->lesson_model->update_news($id, $title);
  }

  public function create()
    {
        $newdata = array(
            'username'  => 'johndoe',
            'email'     => 'johndoe@some-site.com',
            'logged_in' => TRUE
        );

        $this->session->set_userdata($newdata);
    }

    public function read()
    {
        echo $this->session->userdata('logged_in');
    }

    public function set_flash()
    {
        $this->session->set_flashdata('item', 'value');
    }

    public function get_flash()
    {
        echo $this->session->flashdata('item');
    }

    public function config()
    {
//        echo $this->config->item('company');
        $this->load->config('new_conf');
        echo $this->config->item('my_conf');
    }

    public function cache()
    {
        $this->load->driver('cache');

        if (!$this->cache->file->get('foo3')) {
            sleep(3);
            $this->cache->file->save('foo3', 'bar', 10);
        }
        echo $this->cache->file->get('foo3');
    }

    public function bench()
    {
        $this->benchmark->mark('code_start');

        sleep(3);

        $this->benchmark->mark('code_end');

        echo $this->benchmark->elapsed_time('code_start', 'code_end');
    }

    public function mcrypt()
    {
        $this->load->library('encrypt');

        $msg = 'My secret message';

        $encrypted_string = $this->encrypt->encode($msg);

        echo $encrypted_string."<br>";

        echo $this->encrypt->decode($encrypted_string);
    }

    public function form()
    {
        var_dump($this->input->post(NULL, true));

        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required',
            array('required' => "You must provide a %s"));
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('lesson/form');
        }
        else
        {
            $this->load->view('lesson/form');
        }
    }

    public function input()
    {
        var_dump($this->input->post(NULL, true));
    }

    public function table()
    {
        $this->load->library('table');

        $data = array(
            array('Name', 'Color', 'Size'),
            array('Fred', 'Blue', 'Small'),
            array('Mary', 'Red', 'Large'),
            array('John', 'Green', 'Medium')
        );

        echo $this->table->generate($data);
    }

}
