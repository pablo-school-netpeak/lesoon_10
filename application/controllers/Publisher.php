<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//get_instance()->load->iface('controllers/ICrud'); // interface file name

class Publisher extends CI_Controller{

	public function __construct()
	{
					parent::__construct();
					$this->load->model('publisher_model');
					$this->load->helper(array('form', 'url'));
					$this->load->library(array('form_validation','table'));
	}

	public function index() // Read Action
	{

		$this->publisher_model->readPost();

		$posts = $this->publisher_model->readPost();

		//echo $this->table->generate($posts);

		$data = array(
			'posts' => $posts,
		);

		$this->load->view('publisher/read_view', $data);

	}
	public function createPublisher()
	{


		$this->form_validation->set_rules('name_publisher', 'name_publisher', 'required');

		if ($this->form_validation->run() == FALSE)
		{
				$this->load->view('publisher/form');
		} else {
				$data = $this->input->post(NULL, true);
				$this->publisher_model->createPost($data);
				redirect('/publisher', 'refresh');
		}

	}
	public function updatePublisher($id = NULL)
	{

			$post =  $this->publisher_model->getPost($id);

			if ($post !== NULL) {

				$data = array(
					'post' => $post,
				);

				$this->form_validation->set_rules('name_publisher', 'name_publisher', 'required');

				if ($this->form_validation->run() == FALSE)
				{
						$this->load->view('publisher/form', $data);
				} else {
						$data = $this->input->post(NULL, true);

						$data['id'] = $id;

						$this->publisher_model->updatePost($data);
						redirect('/publisher', 'refresh');
				}
			} else {
				redirect('/publisher', 'refresh');
			}

	}

	public function deletePublisher($id = NULL)
  {
		$post =  $this->publisher_model->getPost($id);

		if ($post !== NULL) {
	    $this->publisher_model->deletePost($id);
			redirect('/publisher', 'refresh');
		} else {
			redirect('/publisher', 'refresh');
		}
  }


}
