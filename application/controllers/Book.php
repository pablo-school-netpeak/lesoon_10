<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//get_instance()->load->iface('controllers/ICrud'); // interface file name

class Book extends CI_Controller{

	public function __construct()
	{
					parent::__construct();
					$this->load->model('book_model');
					$this->load->helper(array('form', 'url'));
					$this->load->library(array('form_validation','migration','table','session'));
	}

	public function index() // Read Action
	{
		$this->book_model->readPost();
		$posts = $this->book_model->readPost();

        $publishersId = $this->book_model->getPublishersIds();
		if (!empty($posts)) {
			$data = array(
				'posts' => $posts,
                'publishersId' => $publishersId,
			);
			$this->load->view('book/read_view', $data);
		}
	}

	public function createBook()
	{
		$publishersId = $this->book_model->getPublishersIds();
		$data = array(
			'publishersId' => $publishersId,
		);
        $postData = $this->input->post(NULL, true);
        $errors = array('succes' => false, 'message' => array());
		$this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run()) {
            $errors['succes'] = true;
            $this->book_model->createPost($postData);
		} else {
            foreach ($postData as $key => $value) {
                $errors['message'][$key] = form_error($key);
            }
        }
        echo  json_encode($errors);

	}
    public function getLastBook() {
        $data =  $this->book_model->getLastPost();
        echo json_encode($data);
    }

    public function ajaxGetBook($id = NULL) {
        $id = (int) $id;
        $post =  $this->book_model->getPost($id);
        echo json_encode($post);
    }

	public function updateBook($id = NULL)
	{
	    $id = (int) $id;
        $post =  $this->book_model->getPost($id);

        if ($post !== NULL) {
            $errors = array('succes' => false, 'message' => array());
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('id_publisher', 'Id_publisher', 'required');
            if ($this->form_validation->run()) {
                $errors['succes'] = true;
                $data['id'] = $id;
                $data = $this->input->post(NULL, true);
                $this->book_model->updatePost($data);
            } else {
                foreach ($errors as $key => $value) {
                    $errors['message'][$key] = form_error($key);
                }
            }
            echo json_encode($this->book_model->getPost($id));
        }

	}
	public function deleteBook($id = NULL)
  {
        $post =  $this->book_model->getPost($id);
        if ($post !== NULL) {
            $this->book_model->deletePost($id);
        }
  }
}
