<!doctype html>
<!--suppress ALL -->
<html lang="eng">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <title>School Netpeak</title>
    <style>
        body {
            background-color: #00B8E4;
            color: #fff;
            font-family: 'Open Sans', sans-serif;
        }
        .container {
            width: 80%;
            margin: 0 auto;
            text-align: center;
        }
        h1 {
            display: inline-block;
            vertical-align: middle;
            width: 65%;
            font-size: 36px;

        }
        table {
            width: 100%;
            border-collapse: collapse;
            border: 1px solid #fff;
        }
        td, th {
            padding: 15px;
            text-align: left;
        }
        .field {
          display: block;
          height: 100%;
          text-decoration: none;
          color: #fff;
        }
        .field_edit {
          background: orange;
        }
        .field_delete {
          background: red;
        }
        .field_add {
          background: green;
        }
        .form-wrapp {
          text-align: left;
          display: none;
          position: relative;
        }
        form input,
        form select{
              font-size: 14px;
              padding: 2px 2px 2px 8px;
              border-radius: 4px;
              border: none;
          }
        form button {
           background-color: #F07F1B;
           border: none;
           border-radius: 4px;
           font-size: 15px;
           padding: 10px;
           color: #fff;
           cursor: pointer;form.set-date button
           transition: background-color .3s;
       }
       form button:hover {
           background-color: #d87013;
       }
       .hide_form {
          position: absolute;
          bottom: 10px;
          right: 10px;
          display: inline-block;
          text-decoration: none;
          font-size: 25px;
          color: white;
          border-radius: 50%;
          border: 2px solid #fff;
          width: 35px;
          height: 35px;
          text-align: center;
       }
    </style>
</head>
<body>
    <div class="container">
        <div class="books">
            <table border="1">

                <?php foreach ($posts as $item): ?>
                        <tr>

                          <?php foreach ($item as $key => $value): ?>

                            <td class="<?php echo 'collumn-'.$key; ?>"><?php echo $value ?></td>

                          <?php endforeach; ?>

                          <td><a href="/index.php/book/ajaxGetBook/<?php echo $item['id'] ?>" class="field field_edit">Edit</a></td>
                          <td><a href="/index.php/book/deleteBook/<?php echo $item['id'] ?>" class="field field_delete">Delete</a></td>
                        </tr>
                <?php endforeach; ?>

            </table>
            <a href="#" class="field field_add" >Add</a>
        </div>
        <div class="form-wrapp">
          <form action="/index.php/book/createBook" class="addPost">

            <h5>Set name of the book</h5>
            <input type="hidden" name="id" value="" size="50" />
            <input type="text" name="name" value="" size="50" />

            <h5>Set publisher</h5>
              <select name="id_publisher">
                  <?php foreach ($publishersId as $key => $item): ?>
                      <option value="<?php echo $item['id']; ?>" <?php echo  set_select('id_publisher', $item['id']) ?>  >
                          <?php echo $item['name_publisher']; ?>
                      </option>
                  <?php endforeach; ?>
              </select>
            <button type="submit">Submit</button>
          </form>
          <a href="#" class="hide_form">X</a>
        </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        $('.field_add').on('click',function(){
          $('.form-wrapp').slideDown();
        });
        $('.hide_form').on('click',function(){
          $('.form-wrapp').slideUp();
        });

        $(document).on('submit', '.addPost', function(e){
            e.preventDefault();
            var me = $(this);
            if(confirm('this is create method')){
                $.ajax({
                    url: '/index.php/book/createBook',
                    type: 'POST',
                    data: me.serialize(),
                })
                    .then(function () {
                        return $.get('/index.php/book/getLastBook');
                    })
                    .then(response => {
                        obj = $.parseJSON(response);
                        console.log(obj);
                        id = obj['id'];
                        name = obj['name'];
                        name_publisher = obj['name_publisher'];
                        $('<tr><td class="collumn-id">' + id + '</td><td class="collumn-name">' + name + '</td><td class="collumn-name_publisher">'+ name_publisher+'</td><td><a href="/index.php/book/ajaxGetBook/' + id + '" class="field field_edit">Edit</a></td><td><a href="/index.php/book/deleteBook/' + id + '" class="field field_delete">Delete</a></td></tr>')
                            .insertAfter('table tr:last-of-type');
                    })
                    .done(function () {
                            $('.form-wrapp').slideUp();
                            $(".addPost")[0].reset();

                    });
            }
        });

        $(document).on('click', '.field_edit', function (e) {
            e.preventDefault();
            var me = $(this);
            if ($('tr').hasClass('tmp-tr')) {
                $('tr').removeClass('tmp-tr');
            }
            me.parents('tr').addClass('tmp-tr');
            $.get(me.attr('href'))
                .then(response => {
                    obj = $.parseJSON(response);
                    id = obj['id'];
                    name = obj['name'];
                    id_publisher = obj['id_publisher'];

                    newAction = $('.form-wrapp form').attr('action').replace('createBook', 'updateBook/' + id);

                    $('.form-wrapp form').attr('action', newAction)
                    $('.form-wrapp input[name="id"]').val(id);
                    $('.form-wrapp input[name="name"]').val(name);
                    $('.form-wrapp select[name="id_publisher"] option').removeAttr('selected');
                    $('.form-wrapp select[name="id_publisher"] option[value="'+ id_publisher +'"]').attr('selected', true);

                    $('.form-wrapp').slideDown();
                    $('.form-wrapp form').removeClass('addPost').addClass('updatePost');
                })
        });

        $(document).on('submit', '.updatePost', function(e){
            e.preventDefault();
            var me = $(this);
            $.ajax({
                url: me.attr('action'),
                type: 'POST',
                data: me.serialize(),
            })
                .then(response => {
                    obj = $.parseJSON(response);
                    console.log(obj);
                    id = obj['id'];
                    name = obj['name'];
                    name_publisher = obj['name_publisher'];


                    $('.collumn-id').each(function () {
                        if ($(this).html() == id) {
                            $(this).siblings('.collumn-name').html(name);
                            $(this).siblings('.collumn-name_publisher').html(name_publisher);
                        }
                    })

                })
                .done(function () {
                    $('.form-wrapp').slideUp();
                });

        });

        $(document).on('click', '.field_delete', function (e) {
            e.preventDefault();
            var me = $(this);
            if (confirm('Are you sure you want to delete?')) {
                $.get(me.attr('href'))
                    .then(function () {
                        me.parents('tr').remove();
                    });
            }

        });
    </script>
</body>
</html>
