<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <title>School Netpeak</title>
  <style>
      body {
          background-color: #00B8E4;
          color: #fff;
          font-family: 'Open Sans', sans-serif;
      }

        .container {
            width: 80%;
            margin: 0 auto;
        }

  </style>
</head>
  <body>
    <div class="container">

    <?php echo validation_errors();?>

      <form action="/book/<?php echo isset($post['id']) ? "updateBook/{$post['id']}" : "createBook" ?>" method="post">

        <h5>Set name of the book</h5>
        <input type="hidden" name="id" value="<?php echo set_value('id', @$post['id']); ?>" size="50" />
        <input type="text" name="name" value="<?php echo set_value('name', @$post['name']); ?>" size="50" />

        <h5>set publisher id</h5>

        <select name="id_publisher">
            <?php foreach ($publishersId as $key => $item): ?>
            <option value="<?php echo $item['id']; ?>" <?php echo  set_select('id_publisher', $item['id']) ?> <?php echo ($item['id'] == @$post['id_publisher']) ? "selected" : "" ?> >
              <?php echo $item['id']; ?>
            </option>
          <?php endforeach; ?>
        </select>


        <button type="submit"> Submit</buttom>

      </form>
    </div>

  </body>
</html>
