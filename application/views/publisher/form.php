<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <title>School Netpeak</title>
  <style>
      body {
          background-color: #00B8E4;
          color: #fff;
          font-family: 'Open Sans', sans-serif;
      }
      form input,
      form select{
            font-size: 14px;
            padding: 2px 2px 2px 8px;
            border-radius: 4px;
            border: none;
        }
        form button {
           background-color: #F07F1B;
           border: none;
           border-radius: 4px;
           font-size: 15px;
           padding: 10px;
           color: #fff;
           cursor: pointer;form.set-date button
           transition: background-color .3s;
       }
       form button:hover {
           background-color: #d87013;
       }
        .container {
            width: 80%;
            margin: 0 auto;
        }

  </style>
</head>
  <body>
    <div class="container">

    <?php echo validation_errors();?>

      <form action="/publisher/<?php echo isset($post['id']) ? "updatePublisher/{$post['id']}" : "createPublisher" ?>" method="post">

      <h5>Set name of the book</h5>
      <input type="hidden" name="id" value="<?php echo set_value('id', @$post['id']); ?>" size="50" />
      <input type="text" name="name_publisher" value="<?php echo set_value('name_publisher', @$post['name_publisher']); ?>" size="50" />

      <button type="submit"> Submit</buttom>

      </form>
  </div>

  </body>
</html>
