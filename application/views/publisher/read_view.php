<!doctype html>
<html lang="eng">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <title>School Netpeak</title>
    <style>
        body {
            background-color: #00B8E4;
            color: #fff;
            font-family: 'Open Sans', sans-serif;
        }
        /*form.set-date {
            background-color: #15a4ce;
            border-radius: 3px;
            padding: 7.5px;
        }
        form.set-date input,
        form.set-date select{
            font-size: 14px;
            padding: 2px 2px 2px 8px;
            border-radius: 4px;
            border: none;
        }
        form.set-date button {
            background-color: #F07F1B;
            border: none;
            border-radius: 4px;
            font-size: 15px;
            padding: 10px;
            color: #fff;
            cursor: pointer;form.set-date button
            transition: background-color .3s;
        }
        form.set-date button:hover {
            background-color: #d87013;
        }*/
        .container {
            width: 80%;
            margin: 0 auto;
            text-align: center;
        }
        h1 {
            display: inline-block;
            vertical-align: middle;
            width: 65%;
            font-size: 36px;

        }
        table {
            width: 100%;
            border-collapse: collapse;
            border: 1px solid #fff;
        }
        td, th {
            padding: 15px;
            text-align: left;
        }
        .field {
          display: block;
          height: 100%;
          text-decoration: none;
          color: #fff;
        }
        .field_edit {
          background: orange;
        }
        .field_delete {
          background: red;
        }
        .field_add {
          background: green;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="calculator">
            <div class="calculator_info">
                <h1></h1>
            </div>
            <table border="1">
                <?php foreach ($posts as $item): ?>
                        <tr>
                          <?php foreach ($item as $key => $value): ?>
                            <td><?php echo $value ?></td>
                          <?php endforeach; ?>
                          <td><a href="/publisher/updatePublisher/<?php echo $item['id'] ?>" class="field field_edit">Edit</a></td>
                          <td><a href="/publisher/deletePublisher/<?php echo $item['id'] ?>" class="field field_delete">Delete</a></td>
                        </tr>
                <?php endforeach; ?>

            </table>
            <a href="/publisher/createPublisher" class="field field_add" >Add</a>

        </div>
    </div>
</body>
</html>
