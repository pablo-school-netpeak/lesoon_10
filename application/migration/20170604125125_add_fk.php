<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fk extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_column('books',['CONSTRAINT fk_id FOREIGN KEY (id_publisher) REFERENCES publisher(id)']);
        }

        public function down()
        {
                $this->dbforge->drop_column('books','fk_id');
        }
}
