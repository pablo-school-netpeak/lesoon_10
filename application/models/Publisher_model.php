<?php
get_instance()->load->iface('models/ICrud_model'); // interface file name

class publisher_model extends CI_Model implements ICrud_model {

  public function __construct () {

    parent::__construct();
    $this->load->database();

  }

  public function getPost($id = NULL)
  {
    $id = (int) $id;

    $query = $this->db->select('*')->from('publisher')
    ->where(array('id' => $id))
    ->get();
    return $query->row_array();
  }
  public function createPost($data = array())
  {

    $name = $data['name_publisher'];


    if (!empty($name))
    {
      $this->db->insert('publisher', array(
        'name_publisher' => $name,
      ));
    }


  }

  public function readPost()
  {
    $query = $this->db->query('SELECT * FROM publisher');

    return $query->result_array();
  }

  public function updatePost($post = array())
  {

    $id = (int) $post['id'];

    $name = (string) $post['name_publisher'];

    if (!empty($name)) {
      $this->db->set(array(
        'name_publisher' => $name,));
      $this->db->where('id', $id);
      $this->db->update('publisher');
    }

  }

  public function deletePost($id = NULL) {

    $id = (int) $id;

    if (!empty($id))
    {
      $this->db->delete('publisher', array(
        'id' => $id,
      ));
    }

  }

}
