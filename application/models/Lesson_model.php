<?php
class lesson_model extends CI_Model {

  public function __construct()
  {
          $this->load->database();
  }

  public function get_news($id = NULL)
  {
    $id = (int) $id;

    if (empty($id))
    {
            $query = $this->db->get('news');
            return $query->result_array();
    }

    $query = $this->db->select(array(
      'news.id',
      'news.title',
    ))->from('news')
    ->where(array('id' => $id))
    ->get();
    return $query->row_array();
  }

  public function insert_news($title = '')
  {
    $title = (string) $title;

    if (!empty($title))
    {
      $this->db->insert('news', array(
        'title' => $title,
      ));
    }

  }

  public function delete_news($id = NULL)
  {
    $id = (int) $id;

    if (!empty($id))
    {
      $this->db->delete('news', array(
        'id' => $id,
      ));
    }
  }

  public function update_news($id = NULL, $title = '')
  {
    $id = (int) $id;

    $title = (string) $title;

    if (!empty($id) && !empty($title) )
    {
      $this->db->set('title', $title);
      $this->db->where('id', $id);
      $this->db->update('news');
    }
  }

}
