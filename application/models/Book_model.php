<?php
get_instance()->load->iface('models/ICrud_model'); // interface file name

class book_model extends CI_Model implements ICrud_model {

  public function __construct () {
    parent::__construct();
    $this->load->database();
  }

  public function getPost($id = NULL)
  {
    $id = (int) $id;
    $query = $this->db->select(array(
        'books.id',
        'books.name',
        'books.id_publisher',
        'publisher.name_publisher'
    ))->from('books')
    ->join('publisher','publisher.id = books.id_publisher','left')
    ->where(array('books.id' => $id))
    ->get();
    return $query->row_array();
  }
  public function getLastPost()
  {
      $query = $this->db->select(array(
          'books.id',
          'books.name',
          'books.id_publisher',
          'publisher.name_publisher'
      ))->from('books')
          ->join('publisher','publisher.id = books.id_publisher','left')
          ->order_by('books.id', 'DESC')
          ->limit('1')
          ->get();
      return $query->row_array();
  }
  public function getPublishersIds()
  {
    $query = $this->db->query('SELECT * FROM publisher');
    return $query->result_array();
  }

  public function createPost($data = array())
  {

    $name = $data['name'];
    $id_publisher = $data['id_publisher'];
    if (!empty($name) && !empty($id_publisher)) {
      $query = $this->db->insert('books', array(
        'name' => $name,
        'id_publisher' => $id_publisher,
      ));
      return $query;
    }
  }

  public function readPost()
  {
    $query = $this->db->select(array(
      'books.id',
      'books.name',
      'publisher.name_publisher'
    ))
    ->from('books')
    ->join('publisher','publisher.id = books.id_publisher','left')
    ->order_by('id', 'ASC')
    ->get();
    return $query->result_array();
  }

  public function updatePost($data = array())
  {
    $data = (array) $data;
    if (array_key_exists('name', $data) && array_key_exists('id_publisher', $data) && array_key_exists('id', $data)) {
      $id = (int) $data['id'];
      $name = (string) $data['name'];
      $id_publisher = (int) $data['id_publisher'];
      $this->db->set(array(
        'name' => $name,
        'id_publisher' => $id_publisher
      ))
      ->where('id', $id);
      $query = $this->db->update('books');
      return $query;
    }
  }

  public function deletePost($id = NULL) {
    $id = (int) $id;
    if (!empty($id)) {
      $query = $this->db->delete('books', array(
        'id' => $id,
      ));
      return $query;
    }
  }
}
