<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('newHelper_method'))
{
    function newHelper_method($var = '')
    {
        $var = trim((string) $var);
        return $var;
    }
}
